#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void rainbow_effect(Adafruit_NeoPixel pixels, int num_pixels) {
    while (true) {
        for (long firstPixelHue = 0; firstPixelHue < 65536; firstPixelHue += 256) {
            for (int i = 0; i < pixels.numPixels(); i++) {  // For each pixel in strip...
                int pixelHue = firstPixelHue + (i * 65536L / pixels.numPixels());
                pixels.setPixelColor(i, pixels.gamma32(pixels.ColorHSV(pixelHue)));
            }
            pixels.show();  // Update strip with new contents
            delay(10);      // Pause for a moment
        }
    }
}