#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>
// #include <etl/deque.h>

void development_effect(Adafruit_NeoPixel pixels, int num_pixels) {
    pixels.clear();
    randomSeed(analogRead(A0) * analogRead(A1));

    uint16_t history[pixels.numPixels()];

    while (true) {
        long color = pixels.ColorHSV(random(2000, 22000), 255, 255);

        int before_c = color;
        for (int c = 1; c < pixels.numPixels(); c++) {
            uint16_t history_c = history[c];
            history[c] = before_c;
            before_c = history_c;
        }
        history[0] = color;
        Serial.println(history[0]);
        Serial.println(history[1]);
        Serial.println(history[2]);
        Serial.println("___");
        // if (history.size() > pixels.numPixels() - 5) {
        //     history.pop_back();
        // }
        // Serial.println(history);

        for (int i = 0; i < pixels.numPixels(); i++) {
            pixels.setPixelColor(pixels.numPixels() - i, history[i]);
        }
        pixels.show();
        delay(50);
    }
}