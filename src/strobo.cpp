#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void strobo_effect(Adafruit_NeoPixel pixels, int num_pixels, int off_time, int on_time) {
    int shift = 0;
    while (true) {
        for (int i = 0; i < num_pixels; i++) {
            pixels.setPixelColor(i, pixels.Color(255, 255, 255));
        }
        pixels.show();
        delay(on_time);
        for (int i = 0; i < num_pixels; i++) {
            pixels.setPixelColor(i, pixels.Color(0, 0, 0));
        }
        pixels.show();
        delay(off_time);
    }
}