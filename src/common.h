#include <Arduino.h>

struct RgbColor {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

RgbColor get_rgb_for_rainbow_percentage(int p);