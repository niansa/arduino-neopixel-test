#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>
#include <development.h>
#include <rainbow.h>
#include <multitrain.h>
#include <stacking.h>
#include <strobo.h>
#include <blinds.h>

const int num_pixels = 26;
const int pin = 6;

Adafruit_NeoPixel pixels(num_pixels, pin, NEO_GRB + NEO_KHZ800);

void setup() {
    Serial.begin(9600);
    pixels.begin();
    // multitrain_effect(pixels, 50, pixels.Color(255,0,255), 10);
    // stacking_effect(pixels);
    development_effect(pixels, num_pixels);
    // strobo_effect(pixels, num_pixels, 50, 40);
    // rainbow_effect(pixels, num_pixels);
    // blinds_effect(pixels, 8, 300);
}
