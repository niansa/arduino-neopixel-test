#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void stacking_effect(Adafruit_NeoPixel pixels) {
    uint64_t pixel_to_hue_factor = (int) 65535 / pixels.numPixels();
    while (true) {
        for (size_t i = 0; i < pixels.numPixels(); i++) {
            uint64_t target_pixel = pixels.numPixels() - i;
            uint64_t hue = pixel_to_hue_factor * target_pixel;
            for (size_t j = 0; j < pixels.numPixels() and j < target_pixel; j++) {
                pixels.setPixelColor(pixels.numPixels() - j + 1, pixels.Color(0, 0, 0));
                pixels.setPixelColor(pixels.numPixels() - j, pixels.ColorHSV(hue, 255, 255));
                pixels.show();
                delay(20);
            }
            delay(50);
        };
        delay(200);
        pixels.clear();
    }
}