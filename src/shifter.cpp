#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void shifter_effect(Adafruit_NeoPixel pixels, int num_pixels) {
    int shift = 0;
    while (true) {
        for (int i = 0; i < num_pixels; i++) {
            Serial.println(shift);
            pixels.setPixelColor(i + shift, pixels.Color(255, 0, 0));
            pixels.setPixelColor(i + shift + 1, pixels.Color(0, 255, 0));
            pixels.setPixelColor(i + shift + 2, pixels.Color(0, 0, 255));
            i += 8;
            pixels.show();
        }
        if (shift == 3) {
            shift = 0;
        } else {
            shift++;
        }

        delay(100);
    }
}