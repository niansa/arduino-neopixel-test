#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void multitrain_effect(Adafruit_NeoPixel pixels, float delaytime, uint32_t color, int cut_factor) {
    pixels.clear();

    int shift = 0;

    while (true) {
        uint16_t k, l;
        pixels.clear();
        for (int i = 0; i < pixels.numPixels(); i++) {
            if (i % cut_factor == 0) {
                pixels.setPixelColor(i + shift, color);
            }
        }

        pixels.show();

        shift++;
        if (shift >= cut_factor) {
            shift = 0;
        }
        delay(delaytime);
    }
}