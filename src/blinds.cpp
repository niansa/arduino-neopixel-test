#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Vector.h>
#include <common.h>

void blinds_effect(Adafruit_NeoPixel pixels, int cut_factor, int delaytime) {
    uint32_t colors[] = {pixels.Color(0, 0, 255), pixels.Color(0, 255, 0), pixels.Color(255, 255, 0), pixels.Color(255, 0, 0), pixels.Color(0, 255, 255)};
    unsigned int current_color = 0;
    uint32_t color = pixels.Color(0, 0, 255);
    pixels.clear();

    int shift = 0;

    while (true) {
        // pixels.clear();
        for (int j = 0; j <= cut_factor; j++) {
            for (int i = 0; i < pixels.numPixels(); i++) {
                if (i % cut_factor == 0) {
                    pixels.setPixelColor(i + shift, colors[current_color]);
                    Serial.println(sizeof(colors));
                }
            }

            pixels.show();

            shift++;
            if (shift >= cut_factor) {
                shift = 0;
            }
            delay(delaytime);
        }
        if (current_color < 4) {
            current_color++;
        } else {
            current_color = 0;
        }
        delay(3000);
    }
}