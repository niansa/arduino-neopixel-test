from PIL import Image

def range_map(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def percent_to_rgb(p):
    r=g=b = 0

    if p < 255/2:
        r = 255 - abs(range_map(p, 0, 255/2, -255, 255))


    if p > 255/6 and p < 255/6 * 4:
        g = 255 - abs(range_map(p, 255/6, 255/6*4, -255, 255))
        pass

    if p > 255/2:
        b = 255 - abs(range_map(p, 255/2, 255, -255, 255))
        pass


    return (r, g, b)

image = Image.new(mode="RGB", size=(256,40))
pixels = image.load()
for i in range(255):
    print(i, percent_to_rgb(i))
    for j in range(40):
        pixels[i,j] = percent_to_rgb(i)

image.save("temp.png")
